import { TestBed } from '@angular/core/testing';

import { NewTradeService } from './new-trade.service';

describe('NewTradeService', () => {
  let service: NewTradeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewTradeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
