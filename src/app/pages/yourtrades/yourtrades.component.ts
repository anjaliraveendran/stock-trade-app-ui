import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetTradesComponent } from '../gettrades/gettrades.component';

@Component({
    selector: 'yourtrades-cmp',
    moduleId: module.id,
    templateUrl: 'yourtrades.component.html',  
})

@Injectable({providedIn: 'root'})
export class YourTradesComponent{

  trades: Array<any>;

  constructor(private yourTrades: GetTradesComponent) { }

  ngOnInit() {
    this.yourTrades.getAll().subscribe(data => {
      this.trades = data;
    });

  }

}