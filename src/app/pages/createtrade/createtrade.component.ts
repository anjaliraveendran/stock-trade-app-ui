import { Component, OnInit } from '@angular/core';
import { Create } from 'app/create';
import { NewTradeService } from 'app/new-trade.service';
import { FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';


@Component({
    selector: 'createtrade-cmp',
    moduleId: module.id,
    templateUrl: 'createtrade.component.html'
})

export class CreateTradeComponent implements OnInit{

    create: Create = new Create("", "", 0, 0, "", "");

    form = new FormGroup({
       
        ticker: new FormControl('', Validators.required)

    })

    constructor(private service:NewTradeService){}

    ngOnInit(){
    }

    public postNow(){
        this.service.postTrade(this.create).subscribe((r)=>{console.log(r)});
    }
}
