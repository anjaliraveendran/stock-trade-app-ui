import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Component({
    selector: 'gettrades-cmp',
    moduleId: module.id,
    templateUrl: 'gettrades.component.html'
})

@Injectable({providedIn: 'root'})
export class GetTradesComponent{

    constructor(private http: HttpClient) {
    }
  
    getAll(): Observable<any> {
      return this.http.get('http://citieurlinux8.conygre.com:8080/api/trades/');
    }

}
