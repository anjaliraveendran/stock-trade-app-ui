import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetPortfolioComponent } from '../getportfolio/getportfolio.component';

@Component({
    selector: 'yourportfolio-cmp',
    moduleId: module.id,
    templateUrl: 'yourportfolio.component.html',  
})

@Injectable({providedIn: 'root'})
export class YourPortfolioComponent{
  typeOfInvestment=["Stocks", "Cash", "Bonds"];
  portfolios: Array<any>;

  constructor(private yourPortfolio: GetPortfolioComponent) { }

  ngOnInit() {
    this.yourPortfolio.getAll().subscribe(data => {
      this.portfolios = data;
    });

  }

}