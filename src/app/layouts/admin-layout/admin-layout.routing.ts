import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { CreateTradeComponent } from '../../pages/createtrade/createtrade.component';
import { TableComponent } from '../../pages/table/table.component';
import { TypographyComponent } from '../../pages/typography/typography.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { YourPortfolioComponent } from '../../pages/yourportfolio/yourportfolio';
import { UpgradeComponent } from '../../pages/upgrade/upgrade.component';
import { YourTradesComponent } from 'app/pages/yourtrades/yourtrades.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'createtrade',    component: CreateTradeComponent },
    { path: 'yourtrades',     component: YourTradesComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'yourportfolio',  component: YourPortfolioComponent },
    { path: 'upgrade',        component: UpgradeComponent }
];
