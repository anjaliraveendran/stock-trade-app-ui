import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewTradeService {

  private tradeURL: string;

  

  constructor(private http:HttpClient) { 
    this.tradeURL = 'http://citieurlinux8.conygre.com:8080/api/trades/'
    
  }

    public postTrade(create){
      return this.http.post(this.tradeURL, create, {responseType: 'text' as 'json'})
  }
}
